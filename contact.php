<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Springbate</title>
<!---css--->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!---css--->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Springbate Gym  Responsive web template, Bootstrap Web Templates, Springbate Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!---js--->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<!---js--->
<!--web-fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--JS for animate-->
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="js/wow.min.js"></script>
		<script>
			new WOW().init();
		</script>
	<!--//end-animate-->

</head>
<body>
<!---header-->
	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header wow fadeInLeft animated animated" data-wow-delay="0.4s">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <h1><a class="navbar-brand" href="index.html"><img src="images/e.png"/>  Springbate Gym</a></h1>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right wow fadeInRight animated animated" data-wow-delay="0.4s">
        <li class="active"><a class="nav-in" href="index.php"><span data-letters="Home">Home</span></a></li>
        <li ><a class="nav-in" href="about.php"><span data-letters="Exercises">Exercises</span></a> </li>
        <li><a class="nav-in" href="advice.php"><span data-letters="Health Advice">Health Advice</span></a></li>
        <li><a class="nav-in" href="Blog.php"><span data-letters="Blog">Blog</span></a></li>
        <li><a class="nav-in" href="contact.php"><span data-letters="Gym Locator">Gym Locator</span></a></li>
        <li><a class="nav-in" href="kuda"><span data-letters="Login">Login </span></a></li>

  </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!---banner-->
			<div class="banner wow fadeInDownBig animated animated" data-wow-delay="0.4s">
				<div class="container">
					<h2>Gym Locator</h2>
				</div>
			</div>
	<div class="contact w3l">
	<script src="http://maps.google.com/maps/api/js?sensor=false" 
	type="text/javascript"></script>
</head> 
<body>
<div id="map" style="width: 100%; height: 600px;"></div>

<script type="text/javascript">
var locations = [
['Harare Gym', -17.8091085,31.0371681, 4],
['Mabvuku Gym',-17.7632674,31.0153374, 5],
['Bona Gym', -17.7632674,31.0153699, 3],
['Chatunga Gym', -17.7632674,31.0153374, 2],
['ED Gym', -17.7632674,31.0153374, 1]
];

var map = new google.maps.Map(document.getElementById('map'), {
zoom: 10,
center: new google.maps.LatLng(-17.8091085,31.0371681),
mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {  
marker = new google.maps.Marker({
  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
  map: map
});

google.maps.event.addListener(marker, 'click', (function(marker, i) {
  return function() {
	infowindow.setContent(locations[i][0]);
	infowindow.open(map, marker);
  }
})(marker, i));
}
</script>
	</div>
<!-- //map -->
<!---footer--->
		<!---footer-->
	<?php // include "footer.php";?>
	<!--copy-->
	<!--copy-->
</body>
</html>