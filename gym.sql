/*
Navicat MySQL Data Transfer

Source Server         : bbb
Source Server Version : 50133
Source Host           : localhost:3306
Source Database       : gym

Target Server Type    : MYSQL
Target Server Version : 50133
File Encoding         : 65001

Date: 2018-03-21 04:40:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gymoptions
-- ----------------------------
DROP TABLE IF EXISTS `gymoptions`;
CREATE TABLE `gymoptions` (
  `product` varchar(255) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gymoptions
-- ----------------------------
INSERT INTO `gymoptions` VALUES ('Premium', '100.00', '1');
INSERT INTO `gymoptions` VALUES ('Starter', '20.00', '2');
INSERT INTO `gymoptions` VALUES ('Intermediary', '50.00', '3');

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `amount` double DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `gymoption` varchar(255) DEFAULT NULL,
  `ordernumber` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES ('4', 'd', '1', '44', '1', null);
INSERT INTO `payment` VALUES ('1', 'f@g.gf', '1', 'e3i9a2anu', '2', null);
INSERT INTO `payment` VALUES ('100', 'f@g.gf', '1', 'e3i9a2anu', '3', null);
INSERT INTO `payment` VALUES ('100', 'f@g.gf', '1', 'e3i9a2anu', '4', null);
INSERT INTO `payment` VALUES ('100', 'f@g.gf', '1', 'e3i9a2anu', '5', null);
INSERT INTO `payment` VALUES ('100', 'f@g.gf', '1', 'e3i9e5a4a', '6', null);
INSERT INTO `payment` VALUES ('100', 'f@g.gf', '1', 'e3i9e5a4a', '7', '2018-03-20 07:27:16');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `surname` varchar(40) DEFAULT NULL,
  `sex` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `idnumber` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `access` varchar(30) DEFAULT NULL,
  `studentclass` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'admin', 'male', 'admin@us.org', '0', '0', '0', 'admin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '1', '1', '1', '1', '1');
INSERT INTO `users` VALUES ('3', 'Patience', 'Gahadzikwa', 'female', 'pgahadzikwa@gmail.com', '0772115710', '4 7th Street Ext,\r\nGweru\r\n', '1', 'staff', 'password', '12-12546567-M-12', '1', '09/05/2012', '2', '1');
INSERT INTO `users` VALUES ('9', 'Tendai', 'Mashakda', 'female', 'takawirab@msu.ac.zw', '074636777387', 'Gweru', '4', 'teacher', 'password', '23-45454545-Q-56', '1', '09/24/2012', '2', 'Grade1');
INSERT INTO `users` VALUES ('10', 'CWP', 'Muroiwa', 'male', 'emuroiwa@gmail.com', null, 'adf', '', 'student', 'password', '', '1', '03/13/2018', '3', 'Grade1');
INSERT INTO `users` VALUES ('11', 'dsg', '', null, 'f@f.f', null, null, null, 'f@f.f', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', null, '1', '', '3', null);
INSERT INTO `users` VALUES ('12', 'dg', '', null, 'f@g.gf', null, null, null, 'f@g.gf', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', null, '1', '', '3', null);
